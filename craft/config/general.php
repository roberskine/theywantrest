<?php

/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/general.php
 */

return array(

	'*' => array(
	),

    'iwantrest.com' => array(
        'siteUrl' => 'http://iwantrest.com'
    ),

	'theywantrest.dev' => array(
		'siteUrl'  => 'http://theywantrest.dev'
	),

	'dev.iwantrest.com' => array(
		'siteUrl'  => 'http://dev.iwantrest.com'
	)

);
