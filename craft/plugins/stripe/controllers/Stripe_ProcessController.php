<?php

	namespace Craft;

	class Stripe_ProcessController extends BaseController
	{

		protected $allowAnonymous = true;

		function __construct()
		{
			# code...
		}

		public function actionProcess()
	    {

	    	$this->requirePostRequest();

	    	$data = $_POST['data'];

	    	\Stripe\Stripe::setApiKey("sk_live_4Cgl5eFLKx36cqw55zpz1Mw2");

	    	try {

				$dataStripe = [
					  "amount" => $data['amount']*100, // converts to pennies
	  				  "currency" => "usd",
	  				  "source" => $_POST['stripeToken'], // obtained with Stripe.js
	  				  "description" => "Charge for REST"
				];

				if (isset($data['receipt_email']) && $data['receipt_email'] != '') {
					$dataStripe['metadata'] = [ 'email' => $_POST['receipt_email'] ];
					$dataStripe['receipt_email'] = $data['receipt_email'];
				}


	    		$response = \Stripe\Charge::create($dataStripe);

	    		craft()->userSession->setNotice(Craft::t('Thank you for your donation.'));
				craft()->urlManager->setRouteVariables(array(
               		'donation_amount' => $data['amount'],
               		'receipt_email' => $dataStripe['receipt_email']
            	));


	    	} catch (\Stripe\Error\Card $e) {

	    		$body = $e->getJsonBody();
  				$err  = $body['error'];

  				craft()->userSession->setNotice(Craft::t($err['message']));

	    	} catch (\Stripe\Error\InvalidRequest $e) {

	    		$body = $e->getJsonBody();
  				$err  = $body['error'];

  				craft()->userSession->setNotice(Craft::t($err['message']));

	    	}

	    }

	    public function viewSuccess($response)
	    {

	    }

	}
