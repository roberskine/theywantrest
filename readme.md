###Introduction

Built on:

CraftCMS, a dynamic php-based CMS which can be found here: https://buildwithcraft.com/

While this repository contains multiple directories, the only ones required for the site to function, are `craft` and `html`. The others are source files, using `Grunt` to complile and manage Stylus files, concatonate Javascript, and `Bower` to manage packages used throughout the site. You can find these packages inside the `bower_components` directory.

The site is held in a repo on bitbucket. If you're reading this outside of the Bitbucket, the url is here: https://bitbucket.org/bsley/wewantrest

###How to set up your local environment

–

###How to deploy

–

###CSS Standards

–

###Templating documentation

–
